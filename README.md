# Gradle Plugin for Less

This project aims at providing a [Gradle](https://gradle.org/) plugin providing tasks to easily process [less](https://lesscss.org/#) files.

For now the plugin assumes that the [official less compiler](https://lesscss.org/usage/) is avaliable on the command line.
