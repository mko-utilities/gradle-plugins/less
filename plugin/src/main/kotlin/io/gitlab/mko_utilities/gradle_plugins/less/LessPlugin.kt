/*
 * This file is part of the Gradle plugin for Less.
 * <p>
 * Copyright (c) 2024 by M.K.O.
 * <p>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
package io.gitlab.mko_utilities.gradle_plugins.less

import io.gitlab.mko_utilities.gradle_plugins.less.PluginInfo.PLUGIN_NAME
import io.gitlab.mko_utilities.gradle_plugins.less.PluginInfo.PLUGIN_ID
import io.gitlab.mko_utilities.gradle_plugins.less.PluginInfo.PLUGIN_INFO_TASK_NAME
import io.gitlab.mko_utilities.gradle_plugins.less.PluginInfo.PLUGIN_VERSION
import io.gitlab.mko_utilities.gradle_plugins.less.PluginInfo.PLUGIN_OWNER_ID
import io.gitlab.mko_utilities.gradle_plugins.less.PluginInfo.PLUGIN_EXTENSION_ID
import io.gitlab.mko_utilities.gradle_plugins.less.extensions.LessPluginExtension
import org.gradle.api.DefaultTask
import org.gradle.api.Project
import org.gradle.api.Plugin

/**
 * Main class of the Gradle plugin for Less.
 */
class LessPlugin: Plugin<Project> {

    override fun apply(project: Project) {
        with(project) {
            plugins.apply(LessBasePlugin::class.java)

            val pluginExtension: LessPluginExtension =
                extensions.create(PLUGIN_EXTENSION_ID, LessPluginExtension::class.java)
            // pluginExtension.applyTo(project)

            // Register plugin info task
            tasks.register(PLUGIN_INFO_TASK_NAME, DefaultTask::class.java) {
                group = PLUGIN_NAME
                description = "This tasks outputs information about the version" +
                        " and configuration of the $PLUGIN_NAME plugin."

                it.doLast {
                    println(
                        "This is plugin '$PLUGIN_NAME' ($PLUGIN_ID v$PLUGIN_VERSION)" +
                                " of '$PLUGIN_OWNER_ID'" +
                                " in package '${this@LessPlugin::class.java.`package`.name}'"
                    )
                    println(
                        "$PLUGIN_NAME plugin is configured as follows:" +
                                pluginExtension
                                    .getConfigurationOptionsAsStringList{ n, v -> "   - $n = $v"}
                                    .joinToString(";\n", prefix = "\n", postfix = ".")
                    )
                }
            }
        }
    }
}
