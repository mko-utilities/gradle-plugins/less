package io.gitlab.mko_utilities.gradle_plugins.less.tasks

import io.gitlab.mko_utilities.gradle_plugins.less.extensions.LessPluginExtension
import org.gradle.api.DefaultTask
import org.gradle.api.file.Directory
import org.gradle.api.file.RegularFileProperty
import org.gradle.api.provider.Property
import org.gradle.api.tasks.*
import org.gradle.api.tasks.PathSensitive
import org.gradle.api.tasks.PathSensitivity.ABSOLUTE
import org.gradle.api.tasks.options.Option
import org.gradle.process.ExecOperations
import java.io.File
import javax.inject.Inject

@CacheableTask
abstract class CompileLessFileTask : DefaultTask() {

    /********************************************************************/
    /**  TASK INPUT/OUTPUT PROPERTIES                                  **/
    /********************************************************************/

    /**
     * The executable to use to execute the command.
     */
    @get:Input
    abstract val lessCompilerFilePath: Property<String>

    /**
     * The working directory from which to execute the command.
     */
    @get:Input
    @get:Option(option="working-dir", description = "The working directory from which to compile the Less file.")
    abstract val workingDirectoryPath: Property<String>

    /**
     * The source file to process.
     */
    @get:InputFile
    @get:PathSensitive(ABSOLUTE)
    @get:Option(option="source-file", description = "The source Less file to process.")
    abstract val sourceLessFile: RegularFileProperty

    /**
     * The processed output file
     */
    @get:OutputFile
    @get:Optional
    @get:Option(option="destination-file", description = "The processed CSS output file.")
    abstract val destinationCssFile: RegularFileProperty

    /**
     * The command to execute.
     */
    @get:Input
    @get:Optional
    @get:Option(option="extra-arguments", description = "Extra arguments provided to the compiler.")
    abstract val extraArguments: Property<String>

    init {
        val pluginExtension: LessPluginExtension? =
            project.extensions.findByType(LessPluginExtension::class.java)
        pluginExtension?.also{
            lessCompilerFilePath.convention(it.lessCompilerPath)
            workingDirectoryPath.convention(it.workingDirPath)
        }
    }

    /********************************************************************/
    /**  TASK INTERNAL FIELDS                                          **/
    /********************************************************************/

    @get:Inject
    protected abstract val process: ExecOperations

    /********************************************************************/
    /**  TASK ACTION                                                   **/
    /********************************************************************/

    @TaskAction
    open fun executeCompileLessFileTask() {
        compileLessFile()
    }

    /**
     * Execute the task with the command arguments, provided in {@code #command},
     * appended to the end of the command.
     * @param subcommand the subcommand to execute.
     */
    private fun compileLessFile() {
        val destinationCssFilePath =
            if (destinationCssFile.isPresent)
                destinationCssFile.get().asFile.absolutePath
            else {
                val srcFile: File = sourceLessFile.get().asFile
                "${srcFile.nameWithoutExtension}.css"
            }

        val commandToBeExecuted =
            listOf(
                extraArguments.getOrElse(""),
                sourceLessFile.get().asFile.path,
                destinationCssFilePath
            )
                .filterNot(String::isEmpty)
                .joinToString(" ")

        logger.info(
            "Will process '${sourceLessFile.get()}'" +
                    " with '${lessCompilerFilePath.get()}'" +
                    " generating '${destinationCssFilePath}'."
        )

        val execDir = File(workingDirectoryPath.get())
        execDir.mkdirs()
        // val execFile = File(lessCompilerFilePath.get())
        // val originalPermissions = Files.getPosixFilePermissions(execFile.toPath())
        // Files.setPosixFilePermissions(execFile.toPath(), PosixFilePermissions.fromString("r-xr-xr-x"))
        process.exec {
            it.workingDir = execDir
            it.executable = lessCompilerFilePath.get()
            it.args = commandToBeExecuted.split(" ")
        }
        // Files.setPosixFilePermissions(execFile.toPath(), originalPermissions)
    }
}
