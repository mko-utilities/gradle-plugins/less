package io.gitlab.mko_utilities.gradle_plugins.less.extensions

import org.apache.tools.ant.taskdefs.condition.Os
import org.apache.tools.ant.taskdefs.condition.Os.FAMILY_MAC
import org.apache.tools.ant.taskdefs.condition.Os.FAMILY_UNIX
import org.apache.tools.ant.taskdefs.condition.Os.FAMILY_WINDOWS
import org.gradle.api.Project
import org.gradle.api.file.Directory
import org.gradle.api.file.ProjectLayout
import org.gradle.api.file.RegularFile
import org.gradle.api.logging.Logger
import org.gradle.api.logging.Logging
import org.gradle.api.model.ObjectFactory
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Internal
import java.io.File
import java.lang.Error
import java.net.URI
import java.net.URL
import java.text.MessageFormat

open class LessPluginExtension (
    /**
     * The project for which the plugin extension has been created.
     */
    project: Project
) {

    /**
     * Convenience property to manipulate the project's object factory.
     */
    private val objFact: ObjectFactory = project.objects

    /**
     * Convenience property to manipulate the project's object factory.
     */
    private val projectLayout: ProjectLayout = project.layout

    companion object Constants {
        private val LOGGER: Logger = Logging.getLogger(LessPluginExtension::class.java)
        const val LESS_COMPILER_PATH = "lessc"
    }

    /**********************************************************************/

    /**
     * Convenience method to create properties with an initial value.
     */
    private fun <T> newProperty(clazz: Class<T>, value: T): Property<T> {
        return objFact.property(clazz).also{ it.convention(value) }
    }

    /**
     * Convenience method to create properties with an initial value bound to another provider.
     */
    private fun <T> newProperty(clazz: Class<T>, valueProvider: Provider<T>): Property<T> {
        return objFact.property(clazz).also{ it.convention(valueProvider) }
    }

    /**********************************************************************/

    /**
     * A string representing a path to the less compiler
     */
    @Input
    val lessCompilerPath: Property<String> = newProperty(String::class.java, LESS_COMPILER_PATH)

    /**
     * A string representing a path to the less compiler
     */
    @Input
    val workingDirPath: Property<String> =
        newProperty(
            String::class.java,
            projectLayout.buildDirectory.get().asFile.absolutePath
        )

    /********************************************************************/

    /**
     * Retrieve current plugin configuration as a Map of configuration option names mapped to their current
     * configuration value.
     * @return an immutable map from configuration option names to their current value.
     */
    @Internal // Required by Gradle which assumes any method with a name starting with 'get' and without parameter
              // is an Input/Output property
    fun getConfigurationOptionsAsStringsMap(): Map<String, String> {
        return mapOf(
            "lessCompilerPath" to lessCompilerPath
                .getOrElse("Error: 'lessCompilerPath' configuration option is undefined!"),
            "workingDirPath" to workingDirPath
                .getOrElse("Error: 'workingDirPath' configuration option is undefined!"),
        )
    }

    /**
     * Retrieve current plugin configuration as a List of strings describing options as requested by parameter.
     * @param optionToString a function transforming option name (first parameter) and current value (second
     * parameter) to the desired string in the resulting list.
     * @return an immutable list describing options as specified by the parameter {@code optionToString}.
     */
    fun getConfigurationOptionsAsStringList(optionToString: (String, String) -> String): List<String> {
        return getConfigurationOptionsAsStringsMap().map{ e -> optionToString(e.key, e.value) }
    }

}
