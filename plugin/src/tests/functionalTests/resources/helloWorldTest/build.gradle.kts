import java.nio.file.FileSystems
import java.nio.file.Path
import io.gitlab.mko_utilities.gradle_plugins.less.tasks.CompileLessFileTask

plugins {
    id("io.gitlab.mko575.less")
}

less {
    lessCompilerPath = "lessc"
}

tasks.register<CompileLessFileTask>("helloWorldLessCompilation") {
    lessCompilerFilePath = "lessc"
    workingDirectoryPath = layout.projectDirectory.asFile.absolutePath
    sourceLessFile = layout.projectDirectory.file("src/helloWorld.less")
    destinationCssFile = layout.buildDirectory.file("helloWorld.css")
}

tasks.register<CompileLessFileTask>("helloWorldLessCompilation_withImplicitProperties") {
    sourceLessFile = layout.projectDirectory.file("src/helloWorld.less")
}