/*
 * This Kotlin source file was generated by the Gradle 'init' task.
 */
package io.gitlab.mko_utilities.gradle_plugins.less

import io.gitlab.mko_utilities.gradle_plugins.less.PluginInfo.PLUGIN_NAME
import io.gitlab.mko_utilities.gradle_plugins.less.PluginInfo.PLUGIN_ID
import io.gitlab.mko_utilities.gradle_plugins.less.PluginInfo.PLUGIN_VERSION
import io.gitlab.mko_utilities.gradle_plugins.less.PluginInfo.PLUGIN_INFO_TASK_NAME
import io.gitlab.mko_utilities.gradle_plugins.less.tasks.CompileLessFileTask
import io.gitlab.mko_utilities.gradle_plugins.tests.TestProject
import org.assertj.core.api.Assertions.assertThat
import org.gradle.testfixtures.ProjectBuilder
import kotlin.test.Test
import kotlin.test.assertTrue
import kotlin.test.assertFalse
import kotlin.test.assertNotNull

/**
 * Unit tests for the 'io.gitlab.mko_utilities.gradle_plugins.less' plugin.
 */
class LessPluginTest {

    @Test fun `can apply plugin`() {
        // Create a test project and apply the plugin
        val project = ProjectBuilder.builder().build()
        project.plugins.apply(PLUGIN_ID)

        // Verify the result
        assertTrue(project.plugins.hasPlugin(PLUGIN_ID))
        assertNotNull(project.plugins.findPlugin(PLUGIN_ID))
        assertNotNull(project.plugins.getPlugin(PLUGIN_ID))
        assertNotNull(project.tasks.findByName(PLUGIN_INFO_TASK_NAME))
    }

    @Test fun `can register less compilation task`() {
        // Create a test project and apply the plugin
        val project = ProjectBuilder.builder().build()
        project.plugins.apply(PLUGIN_ID)

        val taskName = "testLessCompilationTask"
        project.tasks.register(taskName, CompileLessFileTask::class.java)

        // Verify the result
        assertFalse(project.tasks.isEmpty())
        assertTrue(project.tasks.named(taskName).isPresent)
    }

    @Test fun `can parse configuration DSL`() {
        val testProject =
            TestProject()
                .initSettingsFile()
                .initBuildFile {
                    writeText("""
                        plugins {
                            id("$PLUGIN_ID")
                        }
                        
                        less {
                            lessCompilerPath = "lessc"
                        }
                    """.trimIndent())
                }
        val test = testProject.run(PLUGIN_INFO_TASK_NAME)

        // Verify the result
        assertTrue(test.output.contains("This is plugin '$PLUGIN_NAME'"))
        assertTrue(test.output.contains("v$PLUGIN_VERSION"))
    }

}
