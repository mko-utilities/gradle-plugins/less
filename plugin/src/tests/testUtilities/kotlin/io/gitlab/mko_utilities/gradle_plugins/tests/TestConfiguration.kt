package io.gitlab.mko_utilities.gradle_plugins.tests

object TestConfiguration {
    const val FUNCTIONAL_TESTS_RESOURCES = "src/tests/functionalTests/resources"
}