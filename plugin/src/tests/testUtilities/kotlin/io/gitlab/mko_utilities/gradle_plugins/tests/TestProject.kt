package io.gitlab.mko_utilities.gradle_plugins.tests

import io.gitlab.mko_utilities.gradle_plugins.less.PluginInfo.PLUGIN_ID
import org.gradle.api.Project
import org.gradle.api.model.ObjectFactory
import org.gradle.api.provider.Property
import org.gradle.testfixtures.ProjectBuilder
import org.gradle.testkit.runner.BuildResult
import org.gradle.testkit.runner.GradleRunner
import java.io.File

class TestProject
constructor(
    private val rootDirectory: File = File.createTempFile("lessPluginTest_", ""),
) {

    companion object {
        private const val SETTINGS_FILE_NAME = "settings.gradle.kts"
        private const val BUILD_FILE_NAME = "build.gradle.kts"
    }

    private val projectBuilder: ProjectBuilder = ProjectBuilder.builder()
    private val project: Property<Project> =  projectBuilder.build().objects.property(Project::class.java)

    private val settingsFile: File by lazy { rootDirectory.resolve(SETTINGS_FILE_NAME) }
    private val buildFile: File by lazy { rootDirectory.resolve(BUILD_FILE_NAME) }

    init {
        rootDirectory.delete()
        rootDirectory.mkdirs()
        projectBuilder.withProjectDir(rootDirectory)
    }

    fun finalizeProject(): TestProject {
        project.set(projectBuilder.build())
        return this
    }

    fun getProject(): Project {
        if (! project.isPresent) finalizeProject()
        return project.get()
    }

    fun getRootDirectory(): File {
        return rootDirectory
    }

    fun initSettingsFile(customizeSettingsFile: File.() -> Unit = {}): TestProject {
        settingsFile.apply {
            customizeSettingsFile()
        }
        return this
    }

    fun initBuildFile(customizeBuildFile: File.() -> Unit = {}): TestProject {
        buildFile.apply {
            writeText("""
                plugins {
                    id("$PLUGIN_ID")
                }
            """.trimIndent())
            customizeBuildFile()
        }
        return this
    }

    fun initWithProjectTemplate(template: File): TestProject {
        template.copyRecursively(rootDirectory)
        return this
    }

    private fun gradleRunner(vararg task: String): GradleRunner {
        finalizeProject()
        return GradleRunner.create().apply {
            forwardOutput()
            withProjectDir(getProject().rootDir)
            withPluginClasspath()
            withDebug(true)
            withArguments(
                "--info",
                "--stacktrace",
                "--warning-mode=fail",
                // "--configuration-cache", // Use of configuration cache generates an error ...
                *task
            )
        }
    }

    fun run(vararg task: String): BuildResult {
        return gradleRunner(*task).build()
    }

    fun runAndFail(vararg task: String): BuildResult {
        return gradleRunner(*task).buildAndFail()
    }

}
